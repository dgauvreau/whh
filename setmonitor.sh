echo "Setting $1 to monitor mode"
iwconfig
ifconfig $1 down
airmon-ng check kill
iwconfig $1 mode monitor
ifconfig $1 up
iwconfig
